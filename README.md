# Toy compiler in F# #

Toy statically typed language compiler using GOLD Parser, Calitha Engine - compiling to .NET CIL

## sample ##


```
#!caml

fun facto(int i):int
    if i < 1 then
        return 1
    else
        return i*facto(i - 1)
    end
end;
write(facto(4));
write(facto(5))

```
