﻿module Utils

open System
open System.IO

type cc= ConsoleColor

let fg(_:TextWriter)(c:cc) =
    Console.ForegroundColor <- c
let bg(_:TextWriter)(c:cc) =
    Console.BackgroundColor <- c
let rst _ = Console.ResetColor()


let hasher = Murmur.Murmur32.Create()
let hash (s:string) =
    let bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(s)
    let hashed = hasher.ComputeHash(bytes)
    BitConverter.ToString(hashed).Replace("-","")
let smallHash (s:string) = (hash s).Substring(0, 6).ToLowerInvariant()
//    System.Text.ASCIIEncoding.ASCII.GetString(hashed)