﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
open System
open System.IO
open System.Reflection
open System.Text.RegularExpressions
open System.Text
open Newtonsoft.Json
open AST
open Utils
open System.Diagnostics

type Compiled = 
    { returnValue : obj
      output : string }

type CompilationResult = 
    | Compiled of Compiled
    | Error

type TestCase = 
    { file : string
      hash : string
      src : string
      desc : string
      result : CompilationResult }

type TestState = 
    { mutable passed : int
      save : bool }

type TestFailure = 
    | ShouldCompile
    | ShouldResolve
    | ShouldRun
    | BadOutput
    | BadError

type TestCaseResult = 
    { case : TestCase
      compilation : Compiling<obj * string> }

let redirectConsole f = 
    let memoryStream = new MemoryStream()
    let writer = new StreamWriter(memoryStream)
    writer.AutoFlush <- true
    Console.SetOut(writer)
    let r = 
        try
            Result.result {
                    let result =  f()
                    memoryStream.Position <- int64 (0)
                    let reader = new StreamReader(memoryStream)
                    let str = reader.ReadToEnd().Replace("\r", "")
                    reader.Dispose()
                    return result, str
            }
        with
            | e -> Result.Failure(Invocation (e.ToString()))
    let cout = new StreamWriter(Console.OpenStandardOutput())
    cout.AutoFlush <- true
    Console.SetOut(cout);
    r


let runTestCase (state : TestState) (case : TestCase) : TestCaseResult = 
    let sb = new StringBuilder()
    let logger = Printf.bprintf sb "%s\n"
    let moduleName = Path.GetFileNameWithoutExtension case.file
    
    let run = 
        Result.result { 
                let! mi = Program.compile logger moduleName state.save case.src
                let! res, output = redirectConsole (fun f -> mi.Invoke(null, [||]))
                return res, output
        }
    { case = case
      compilation = run }

let analyzeTestResult (state : TestState) (result : TestCaseResult) = 
    let passed r = 
        printfn "%a%s%t\t%a%s%t\n%s\t%s" fg cc.Green "PASSED" rst fg cc.Cyan 
            (Path.GetFileNameWithoutExtension(r.case.file)) rst r.case.hash r.case.desc
    let failed r color reason = 
        printfn "%a%s%t\t%a%s%t\n%s\t%s" fg color reason rst fg cc.Cyan (Path.GetFileNameWithoutExtension(r.case.file)) 
            rst r.case.hash r.case.desc
    let diff e a = printfn "\texpected: '%s'\n\t  actual: '%s'" e a
    match result.case.result, result.compilation with
    | Compiled er, (Result.Success(ar, output)) -> 
        if er.output = output then 
            state.passed <- state.passed + 1
            passed result
        else 
            failed result cc.Yellow "OUTPUT"
            diff er.output output
    | Error, (Result.Failure f) -> 
        state.passed <- state.passed + 1
        passed result
    | Compiled er, (Result.Failure f) -> 
        failed result cc.Red "BUILD"
        match f with
        | Parsing(msg, loc) -> printfn "\t%O %s\n%s" loc msg (AST.fillLoc result.case.src loc)
        | Resolving(msg, loc) -> printfn "\t%O %s\n%s" loc msg (AST.fillLoc result.case.src loc)
        | Codegen(msg) -> printfn "\t%s" msg
        | Invocation(msg) -> printfn "\t%s" msg
    | _ -> failed result cc.DarkMagenta "?"

let join<'a> (sep : string) (lines : seq<'a>) = String.Join(sep, lines)

let testFile (state : TestState) path hash = 
    let txt = File.ReadAllText path
    let lines = txt.Split([| "\n" |], StringSplitOptions.None)
    let metaIndex = lines |> Array.findIndex (fun l -> l.StartsWith("/*METADATA"))
    
    let src = 
        lines
        |> Seq.take metaIndex
        |> join "\n"
    
    let rawMetaData = 
        lines
        |> Seq.skip (metaIndex + 1)
        |> Seq.takeWhile (fun l -> not (l.Contains("*/")))
        |> join "\n"
    
    let metaData = rawMetaData.Replace("\r", "")
    
    let testCase = 
        { JsonConvert.DeserializeObject<TestCase>(metaData) with file = path
                                                                 src = src
                                                                 hash = hash }
    //    printfn "%s" (JsonConvert.SerializeObject(testCase, Formatting.Indented))
    runTestCase state testCase

let getTestDir() = 
    let testcases = "TestCases"
    let mutable cur = Path.GetFullPath(".")
    while not (Directory.Exists(Path.Combine(cur, testcases))) do
        cur <- Path.GetDirectoryName(cur)
    let dir = Path.Combine(cur, testcases)
    dir

let listAll() = 
    let dir = getTestDir()
    //    printfn "%A" dir
    Directory.EnumerateFiles(dir, "*.tiny", SearchOption.AllDirectories)
    |> Array.ofSeq
    |> Array.map (fun p -> smallHash (Path.GetFileName(p)), p)

let runAll (state : TestState) files = 
    files
    |> Array.map (fun (h, f) -> testFile state f h)
    |> Array.iter (analyzeTestResult state)
    printfn "\n---------------------"
    printfn "TOTAL: %a%i/%i%t" fg (if state.passed = files.Length then cc.Green
                                   else cc.Red) state.passed files.Length rst

[<EntryPoint>]
let main argv = 
    let sw = Stopwatch.StartNew()
    
    let state = 
        { passed = 0
          save = false }
    
    let all = 
        if argv |> Array.exists (fun x -> x = "all") then listAll()
        else 
            let fname = argv.[0]
            let file = Path.Combine(getTestDir(), fname)
            let h = Utils.smallHash fname
            [| (h, file) |]
    
    let map = Map.ofArray all
    runAll state all
    sw.Stop()
    printfn "time: %i" sw.ElapsedMilliseconds
    let mutable input = Console.ReadLine()
    while not (String.IsNullOrEmpty(input)) do
        let i = input
        all |> Array.filter (fun (h, f) -> i = "*" || h.StartsWith(i)) |> runAll { passed = 0; save = true }
        input <- Console.ReadLine()
    0 // return an integer exit code
