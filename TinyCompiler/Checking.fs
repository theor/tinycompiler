﻿module Checking

open System.Collections.Generic
open AST
open Result
open System.Reflection.Emit

/// <summary>
/// Can cast type tfrom to type tto
/// </summary>
/// <param name="tfrom">Type to cast</param>
/// <param name="tto">Target type</param>
let castable (tfrom : Type) (tto : Type) : bool =
    match tto,tfrom with
    | (tid,t),(tfid,tf) when t.IsArray -> tto = AnyType || (tf.IsArray && t.GetElementType().IsAssignableFrom(tf.GetElementType()))
    | _ -> tto = AnyType || tfrom = tto

let rec resolve (x : AstNode) newScope : Resolving<AstNode> = 
    match x with
    | FunDef(Sym({ body = body } as fd)) -> 
        match body with
        | Fun(b, s) -> 
            let scope = newScope (fd)
            result { let! r = resolve_scope scope b
                     return FunDef(Val({ fd with body = Fun(r, scope) })) }
        | Prim _ -> Failure(sprintf "Should not try to resolve Prim %A" x, NoLoc)
    | _ -> Failure(sprintf "Cannot resolve scope owner %A" x, NoLoc)

and getBinOpType (s : IScope) (op : BinOp) : Type * Type * Type = 
    match op with
    | ADD | SUB | MUL | DIV -> IntType, IntType, IntType
    | LT | LTE | GT | GTE -> IntType, IntType, BoolType
    | EQ | NE -> AnyType, AnyType, BoolType

and typeExpr (s : IScope) (e : Expr) : Resolving<Expr> = 
    match e with
    | Typed _ -> Success e
    | Untyped(ue, loc) -> 
        match ue with
        | Literal _ as lit -> Success(Typed(lit, IntType, loc))
        | Boolean _ as b -> Success(Typed(b, BoolType, loc))
        | BinOp(op, a, b) as bb -> 
            match a, b with
            | Typed(_, ta, loca), Typed(_, tb, locb) -> 
                let (ea, eb, ret) = getBinOpType s op
                match castable ta ea, castable tb eb with
                | true, true -> Success(Typed(bb, ret, loc))
                | false, true -> error loc (BadOpOperandType(op, Some(ea, ta), None))
                | true, false -> error loc (BadOpOperandType(op, None, Some(eb, tb)))
                | false, false -> error loc (BadOpOperandType(op, Some(ea, ta), Some(eb, tb)))
            | _ -> failwith ("untyped val")
        | ArrayAccess(arr, idx) -> match arr with
                                   | Typed(_, (Type(tid),t), locarr) -> Success(Typed(ue, (Type(tid.TrimEnd(']').TrimEnd('[')), t.GetElementType()), locarr))
                                   | _ -> failwith ("untyped val")
        | String _ as s ->Success(Typed(s, StringType, loc))

and assignTyped (s : IScope) (vd : VarDecl) (e : Expr) : Resolving<AstNode> = 
    let vtype, name, _ = vd
    
    let etype, loc = 
        match e with
        | Typed(a, etype, loc) -> etype,loc
        | _ -> failwith ("Asd")
    if vtype = etype then Success(Assignment(Val(vd), e))
    else error loc (BadAssignmentType(name, vtype, etype))
and assignArrayTyped (s : IScope) (ra : Expr) (ri : Expr) (rv : Expr) : Resolving<AstNode> = 
    match ra,ri,rv with
    | Typed(_,ta,_),Typed(_,ti,li),Typed(_,tv,_) ->
        if ti <> IntType then error li (ArrayIndexType(ti))
        elif MakeArrayType tv = ta then
            Success(ArrayAssignment(ra, ri, rv))
        else
            error li (BadAssignmentType(ID("asd"), tv, ta))
and resolve_expr_scope (s : IScope) (Untyped(e,loc) : Expr) : Resolving<Expr> = 
    match e with
    | Call(c) -> 
        result { 
            let! rf, rargs, rtparams = s.ResolveFunction loc c
            let! rt = getVal rf.ret
            if List.length rargs <> List.length rf.parameters then
                return! error loc (WrongNumberOfArguments(rf, rargs.Length))
            else
                let! checkedRArgs = 
                    rargs
                    |> List.map tryGetTyped
                    |> List.zip (List.map getVal rf.parameters)
                    |> List.map (fun (sp, sa) -> 
                           result { 
                               let! (pt,pname,_) = sp
                               let! e,et,loc = sa
                               if castable et pt then return Typed(e,et, loc)
                               else return! error loc (BadArgType(rf, pname, pt, et))
                           })
                    |> Result.flatten
                return Typed(Call(Val(rf, checkedRArgs, rtparams)), rt, loc)
        }
    | Var(v) -> result { let! (vt, vid, vkind) as rv = s.ResolveVar v
                         return Typed(Var(Val(rv)), vt, loc) }
    | BinOp(op, a, b) -> result { let! ra = resolve_expr_scope s a
                                  let! rb = resolve_expr_scope s b
                                  return! typeExpr s (Untyped(BinOp(op, ra, rb), loc)) }
                                  
    | NewArray(t, size) -> result { let! rt = s.ResolveType(t)
                                    let! re = resolve_expr_scope s size
                                    return Typed(NewArray(Val(rt), re), MakeArrayType rt, loc) }
    | ArrayAccess(arr, idx) -> result { let! rarr = resolve_expr_scope s arr
                                        let! tarr = typeExpr s rarr
                                        let! ridx = resolve_expr_scope s idx
                                        return! typeExpr s (Untyped(ArrayAccess(rarr, ridx), loc)) }
    | _ -> result { return! (typeExpr s (Untyped(e, loc))) }

and resolve_scope (s : IScope) (x : AstNode) : Resolving<AstNode> = 
    match x with
    | Expr(e) -> result { let! re = resolve_expr_scope s e
                          return Expr(re) }
    | Decl(d) -> result { let! resd = s.ResolveVarDecl(d)
                          return Decl(Val(resd)) }
    | Stmts(l) -> 
        let lr = l |> List.map (resolve_scope s)
        result { let! lr2 = Result.flatten lr
                 return Stmts(lr2) }
    | Assignment(a, e) -> result { let! ar = s.ResolveVar(a)
                                   let! er = resolve_expr_scope s e
                                   let! assign = assignTyped s ar er
                                   return assign }
    | ArrayAssignment(a, i, v) -> result { let! ra = resolve_expr_scope s a
                                           let! ri = resolve_expr_scope s i
                                           let! rv = resolve_expr_scope s v
                                           return! assignArrayTyped s ra ri rv }
    | FunDef(fd) -> result { let! rfd = s.ResolveFunctionDef fd
                             return FunDef(Val(rfd)) }
    | Return(r) -> 
        match r with
        | None -> Success x
        | Some (Untyped(_,loc) as r) -> result { let! re = resolve_expr_scope s r
                                                 let! exprType = getType(re)
                                                 let! expectedRetType = getVal s.CurrentFunction.ret
                                                 if expectedRetType = VoidType && exprType <> VoidType then
                                                    return! error loc  (ReturnValueInVoidFunction s.CurrentFunction)
                                                 elif not(castable exprType expectedRetType) then
                                                    return! error loc  (BadReturnValueType(s.CurrentFunction, expectedRetType, exprType))
                                                 else
                                                    return Return(Some re) }
        | _ -> failwith("wtf")
    | IfThenElse(e, sTrue, sFalse) -> 
        result { 
            let! re = resolve_expr_scope s e
            let! rTrue = resolve_scope s sTrue
            match sFalse with
            | None -> return IfThenElse(re, rTrue, None)
            | Some(sFalse) -> let! rFalse = resolve_scope s sFalse
                              return IfThenElse(re, rTrue, Some rFalse)
        }
    | RepeatUntil(c,st) ->
        result {
            let! rc = resolve_expr_scope s c
            let! rst = resolve_scope s st
            return RepeatUntil(rc, rst)
        }
//    | _ -> Failure(sprintf "Cannot resolve scope user %A" x, NoLoc)

let getMIT<'a> (mname:string) (args:System.Type array) = typeof<'a>.GetMethod(mname, args)

let mutable primuid = 1
type ResolvingScope(parent : ResolvingScope option, cur : FunDef) = 
    let parent : ResolvingScope option = parent
    let mutable currentFunction : FunDef = cur
    
    let primitives() = 

        let getMI (t:System.Type) (mname:string) (args:System.Type array) = t.GetMethod(mname, args)

        let make (name:string) (args:Type list) (ret:Type) (defMaker: ILGenerator -> unit) =
            { uid=(primuid <- primuid + 1; primuid)
              name = ID(name)
              parameters = args |> List.mapi (fun i a -> Val(a, ID("a" + i.ToString()), Arg)) // [ Val(BoolType, ID("x"), Arg) ]
              typeParams = [ ]
              ret = Val(ret)
              body = Prim(defMaker) }
        let fromMI (name:string) (args:Type list) (ret:Type) (mi:System.Reflection.MethodInfo) =
           make name args ret (fun il -> il.Emit(OpCodes.Call, mi))
        let d = Dictionary<ID, FunDef list>()

        let addOverloads (name:string) dm (defMakers) = 
            let f = dm name 
            d.Add(ID(name), defMakers f |> List.ofArray)
        let createArray = getMIT<System.Array> "CreateInstance" [|typeof<System.Type>;typeof<int>|]
//        d.Add(ID("newarr"),
//              { name = ID("newarr")
//                parameters = [ Val(IntType, ID("x"), Arg) ]
//                typeParams = [Type("T")]
//                ret = Val(VoidType)
//                body = Prim(createArray) } :: [])

        addOverloads "write" fromMI
            (fun f -> [| f []           VoidType (getMIT<System.Console> "WriteLine" [| |]);
                         f [BoolType]   VoidType (getMIT<System.Console> "WriteLine" [| typeof<bool> |]);
                         f [IntType]    VoidType (getMIT<System.Console> "WriteLine" [| typeof<int> |]);
                         f [StringType] VoidType (getMIT<System.Console> "WriteLine" [| typeof<string> |])
                      |]);
//         addOverloads "sort" fromMI
//            (fun f -> [| f []           VoidType (getMIT<System.Array> "Sort" [| |]) |]);
        addOverloads "len" make
            (fun f -> [| f [MakeArrayType AnyType] IntType (fun il -> il.Emit(OpCodes.Ldlen); il.Emit(OpCodes.Conv_I4))|]);
        d
    
    let primitiveTypes() = 
        let d = Dictionary<TID, Type>()
        let addtype str t = d.Add(Type(str), (Type(str), t))
        let addAstType (tid, t) = d.Add(tid, (tid, t))
        addAstType IntType
        addAstType BoolType
        addAstType StringType
        addAstType VoidType
        addAstType AnyType
        d
    
    let variables = Dictionary<ID, VarDecl>()
    let types = primitiveTypes()
    let functions = primitives()
    
    member this.TryGetFunction (loc:Location) (f : ID) : Result<FunDef list, LocationError> = 
        if f = currentFunction.name then Success (currentFunction :: [])
        else 
            match functions.TryGetValue f with
            | true, fdef -> Success(fdef)
            | false, _ -> 
                match parent with
                | Some p -> p.TryGetFunction loc f
                | None -> error loc (UnknownFunction f)

    member this.ResolveOverload (loc:Location) (fundefs:FunDef list) (args:Expr list): Resolving<FunDef> =
        let goodArgNb = fundefs |> List.filter (fun x -> x.parameters.Length = args.Length) // |> Result.ofOption ("No overload", None)
        match goodArgNb.Length with
        | 0 -> Failure("",loc)
        | 1 -> goodArgNb |> List.head |> Success
        | _ -> goodArgNb
               |> List.tryFind (fun x -> 
                                 let res = args
                                           |> List.map tryGetTyped
                                           |> List.zip (List.map getVal x.parameters)
                                           |> List.forall (fun (sp, sa) -> 
                                               let res = result { 
                                                   let! (pt,pname,_) = sp
                                                   let! e,et,loc = sa
                                                   if castable et pt then return true
                                                   else return false
                                               }
                                               match res with | Success x -> x | _ -> false)
                                 res)
               |> Result.ofOption (error loc (NoMatchingOverload fundefs))
    
    interface IScope with
        member x.CurrentFunction
            with get (): FunDef = currentFunction
            and set (v: FunDef): unit = 
                currentFunction <- v
        
        member this.ResolveType r = 
            let resolveTID tid = 
                match types.TryGetValue(tid) with
                | true, t -> Success(t)
                | false, _ -> 
                    match parent with
                    | Some p -> (p :> IScope).ResolveType (Sym tid)
                    | None -> Failure(sprintf "Cannot resolve type %A" tid, NoLoc)
            match r with 
            | Sym(Type(tid) as t) -> resolveTID t
            | Sym(Array(tid)) -> result { let! st = (this :> IScope).ResolveType (Sym tid)
                                          return MakeArrayType st}
            | Val(t) -> Success t
        
        member this.ResolveArgDecl r = this.VarArgDecl r Arg
        member this.ResolveVarDecl r = this.VarArgDecl r Local
        
        member this.ResolveVar r = 
            match r with
            | Sym(id) -> 
                match variables.TryGetValue id with
                | true, vd -> Success(vd)
                | false, _ -> Failure(sprintf "Could not resolve var '%A'" id, NoLoc)
            | _ -> Failure("Already Resolved", NoLoc)
        
        member this.ResolveFunction loc r = 
            match r with
            | Sym(f, args, tparams) -> 
                result { 
                    let! rargs = args
                                 |> List.map (resolve_expr_scope this)
                                 |> Result.flatten
                    let! fdefs = this.TryGetFunction loc f
                    let! fdef = this.ResolveOverload loc fdefs rargs
                    //                    let argTypes = rargs |> List.map (
                    return (fdef, rargs, tparams)
                }
            | _ -> Failure("Already Resolved", NoLoc)
        
        member this.ResolveFunctionDef r = 
            match r with
            | Sym({ name = name; body = body; parameters = parameters } as fd) -> 
                match body with
                | Fun(fbody, loc) -> 
                    let fd = result { 
                                let! ret = (this :> IScope).ResolveType fd.ret
                                let subScope = ResolvingScope(Some this, {fd with ret = Val(ret)})
                                let! rparams = parameters
                                               |> List.map (subScope :> IScope).ResolveArgDecl
                                               |> Result.flatten
                                let rparams2 = rparams |> List.map Val
                                (subScope :> IScope).CurrentFunction <- {fd with ret = Val(ret); parameters = rparams2}
                                let! rfbody = resolve_scope subScope fbody
                                let fd = 
                                    { fd with body = Fun(rfbody, subScope)
                                              parameters = rparams2
                                              ret=Val(ret) }
                                return fd
                            }
                    Result.bind (fun fd -> 
                                    match functions.TryGetValue(name) with
                                    | true, l -> functions.Item(name) <- fd :: l;
                                    | false, _ -> functions.Add(name, fd :: [])
                                    Success fd) fd
                    
                | Prim(_) -> Failure("Def prim", NoLoc)
            | _ -> Failure("Already Resolved", NoLoc)
    
    member this.VarArgDecl r kind = 
        match r with
        | Sym(tid, id) -> 
            result { 
                let! t = (this :> IScope).ResolveType(Sym(tid))
                let vd = (t, id, kind)
                variables.Add(id, vd)
                return vd
            }
        | _ -> Failure("Already Resolved", NoLoc)

let typecheck (x : AstNode) : Compiling<AstNode> =
    match resolve x (fun fd -> ResolvingScope(None, fd)) with
    | Success s -> Success s
    | Failure f -> Failure(Resolving f)

///////////////////////////////////////////////////////////////////////////
let rec flatten_rec (x : AstNode) : AstNode list = 
    match x with
    | Stmts(l) -> List.collect flatten_rec l
    | x -> (flatten x) :: []

and flatten (x : AstNode) : AstNode = 
    match x with
    | Stmts(la) -> (Stmts(List.collect flatten_rec la))
    //    | Program(p, s) -> Program(flatten p, s)
    | FunDef(Sym({ body = Fun(body, scope) } as fd)) -> FunDef(Sym({ fd with body = Fun(flatten body, scope) }))
    | _ -> x
