﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
open System.IO
open Result
open Parsing
open Parser

let src2 = @"
FUN square(int i):int
    IF i < 1 THEN
        return 1
    ELSE
        return i*square(i - 1)
    END
END;
FUN f(int i, int j):int
    write(i + j);
    return i + j
END;
write(f(42, 5));
write(square(4))"
let srcBool = @"
bool b := true;
IF b THEN
    write(42)
ELSE
    write(0)
END
"
let opTests = @"
write(1);
write(1 < 2);
write(2 < 1);
write();
write(1 <= 1);
write(1 <= 2);
write(2 <= 1)
"
let srcOverloading = @"
fun f() write() end;
fun f(int i) write(i) end;
fun f(bool b) write(b) end;
f(1);
f();
f(true)
"
let srcTypeError = @"bool b := -42"
let srcTypeError2 = @"fun f(int i, int j):int
    int sum := i + j;
    write(sum);
    return sum
end;
f(-7, 4)
"
let srcString = @"
string s := ""abc"";
write(s);

fun isTrue(bool b):string
    if b then return ""is true"" else return ""is false"" end
end;
write(isTrue(true));
write(isTrue(false))
"

let srcArray = @"
int[] array;
newarr<int>(2);
write(""array"")
"
//write 7 + 9 + x"
//let src3 = @"FUN f(int i, int j):int write(i);write(j); return i + j END;f(7, 9); write(0)"

let cprintf c fmt = 

    Printf.kprintf 
        (fun s -> 
            let old = System.Console.ForegroundColor 
            try 
              System.Console.ForegroundColor <- c;
              System.Console.Write s
            finally
              System.Console.ForegroundColor <- old) 
        fmt
        
// Colored printfn
let cprintfn c fmt = 
    cprintf c fmt;
    printfn ""

let log logger format = Printf.kprintf logger format
let compile (logger) modulename (save:bool) src =
    let ms = new MemoryStream()
    let x = new MyParser("Tiny.cgt")
    result {
        let! parsed = x.Parse(src)
        let flattened = Checking.flatten parsed
//        log logger "------------------- FLATTENED\n%O" flattened;
        let! resolved = Checking.typecheck flattened 
        log logger "------------------- RESOLVED\n%A" resolved;
        let asm,t = CodeGen.getTypeBuilder (modulename + ".exe")
        log logger "TYPE BUILDER";
        let! compiled = CodeGen.compileFunction asm t resolved
//        log logger "------------------- COMPILED\n%A" compiled;
        do if save then asm.Save(modulename + ".exe")
        return compiled
//        log logger "------------------- SAVED";
//        try
//            let result = (compiled.Invoke(null, [| |]))
////            log logger "------------------- RESULT\n%A" result;
//            return result
//        with
//            | e  -> return e(sprintf "%A" e, None)
    }

[<EntryPoint>]
let main argv = 
    Result.test();
    let src = srcArray
    let r = compile (printfn "%s")  "test" true src
    
    match r with
    | Failure(AST.Parsing(msg, loc)) -> cprintf System.ConsoleColor.Red "%O Error: %sin:\n%s\n" loc msg (AST.fillLoc src loc)
    | Failure(AST.Resolving(msg, loc)) -> cprintf System.ConsoleColor.Red "%O Error: %sin:\n%s\n" loc msg (AST.fillLoc src loc)
    | Failure _ -> failwith "unknown error"// cprintf System.ConsoleColor.DarkRed "%O Error: %sin:\n%s\n" loc msg (AST.fillLoc src AST.NoLoc)
    | Success r -> printfn "done: %A" r;
    0
