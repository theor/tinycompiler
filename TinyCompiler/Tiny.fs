﻿module Parsing

open System
open System.IO
open System.Runtime.Serialization
open com.calitha.goldparser.lalr
open com.calitha.commons

type SymbolConstants =    
| SYMBOL_EOF              =  0 // (EOF)
| SYMBOL_ERROR            =  1 // (Error)
| SYMBOL_COMMENT          =  2 // Comment
| SYMBOL_WHITESPACE       =  3 // Whitespace
| SYMBOL_TIMESDIV         =  4 // '*/'
| SYMBOL_DIVTIMES         =  5 // '/*'
| SYMBOL_MINUS            =  6 // '-'
| SYMBOL_EXCLAMEQ         =  7 // '!='
| SYMBOL_LPAREN           =  8 // '('
| SYMBOL_RPAREN           =  9 // ')'
| SYMBOL_TIMES            = 10 // '*'
| SYMBOL_COMMA            = 11 // ','
| SYMBOL_DIV              = 12 // '/'
| SYMBOL_COLON            = 13 // ':'
| SYMBOL_COLONEQ          = 14 // ':='
| SYMBOL_SEMI             = 15 // ';'
| SYMBOL_LBRACKET         = 16 // '['
| SYMBOL_LBRACKETRBRACKET = 17 // '[]'
| SYMBOL_LBRACKETPIPE     = 18 // '[|'
| SYMBOL_RBRACKET         = 19 // ']'
| SYMBOL_PIPERBRACKET     = 20 // '|]'
| SYMBOL_PLUS             = 21 // '+'
| SYMBOL_LT               = 22 // '<'
| SYMBOL_LTEQ             = 23 // '<='
| SYMBOL_EQ               = 24 // '='
| SYMBOL_GT               = 25 // '>'
| SYMBOL_GTEQ             = 26 // '>='
| SYMBOL_ELSE             = 27 // else
| SYMBOL_END              = 28 // end
| SYMBOL_FALSE            = 29 // false
| SYMBOL_FUN              = 30 // fun
| SYMBOL_ID               = 31 // ID
| SYMBOL_IF               = 32 // if
| SYMBOL_NEW              = 33 // new
| SYMBOL_NUMBER           = 34 // Number
| SYMBOL_REPEAT           = 35 // repeat
| SYMBOL_RETURN           = 36 // return
| SYMBOL_STRING           = 37 // String
| SYMBOL_THEN             = 38 // then
| SYMBOL_TRUE             = 39 // true
| SYMBOL_UNTIL            = 40 // until
| SYMBOL_ARGLIST          = 41 // <arglist>
| SYMBOL_ARGLIST2         = 42 // <arglist2>
| SYMBOL_ARRAYACCESS      = 43 // <array access>
| SYMBOL_ARRAYASSIGNSTMT  = 44 // <array assign stmt>
| SYMBOL_ASSIGNSTMT       = 45 // <assign stmt>
| SYMBOL_BOOL             = 46 // <bool>
| SYMBOL_CALL             = 47 // <call>
| SYMBOL_COMP             = 48 // <comp>
| SYMBOL_EXP              = 49 // <exp>
| SYMBOL_FACTOR           = 50 // <factor>
| SYMBOL_FUNDEFRETTYPE    = 51 // <fundef ret type>
| SYMBOL_FUNDEFSTMT       = 52 // <fundef stmt>
| SYMBOL_IFSTMT           = 53 // <if stmt>
| SYMBOL_NEWARRAY         = 54 // <new array>
| SYMBOL_PARAMLIST        = 55 // <paramlist>
| SYMBOL_PARAMLIST2       = 56 // <paramlist2>
| SYMBOL_PROGRAM          = 57 // <program>
| SYMBOL_REPEATSTMT       = 58 // <repeat stmt>
| SYMBOL_RETURNSTMT       = 59 // <return stmt>
| SYMBOL_SIMPLEEXP        = 60 // <simple exp>
| SYMBOL_STMT             = 61 // <stmt>
| SYMBOL_STMTSEQ          = 62 // <stmt seq>
| SYMBOL_TERM             = 63 // <term>
| SYMBOL_TYPE             = 64 // <type>
| SYMBOL_TYPEPARAMLIST    = 65 // <typeparamlist>
| SYMBOL_TYPEPARAMLIST2   = 66 // <typeparamlist2>
| SYMBOL_TYPEPARAMS       = 67 // <typeparams>
| SYMBOL_VARDEFSTMT       = 68 // <vardef stmt>

type RuleConstants =
| RULE_PROGRAM                                      =  0 // <program> ::= <stmt seq>
| RULE_STMTSEQ_SEMI                                 =  1 // <stmt seq> ::= <stmt seq> ';' <stmt>
| RULE_STMTSEQ                                      =  2 // <stmt seq> ::= <stmt>
| RULE_STMT                                         =  3 // <stmt> ::= <if stmt>
| RULE_STMT2                                        =  4 // <stmt> ::= <repeat stmt>
| RULE_STMT3                                        =  5 // <stmt> ::= <assign stmt>
| RULE_STMT4                                        =  6 // <stmt> ::= <fundef stmt>
| RULE_STMT5                                        =  7 // <stmt> ::= <vardef stmt>
| RULE_STMT6                                        =  8 // <stmt> ::= <call>
| RULE_STMT7                                        =  9 // <stmt> ::= <return stmt>
| RULE_TYPE_ID                                      = 10 // <type> ::= ID
| RULE_TYPE_ID_LBRACKETRBRACKET                     = 11 // <type> ::= ID '[]'
| RULE_PARAMLIST2                                   = 12 // <paramlist2> ::= 
| RULE_PARAMLIST2_COMMA                             = 13 // <paramlist2> ::= ',' <paramlist>
| RULE_PARAMLIST                                    = 14 // <paramlist> ::= 
| RULE_PARAMLIST_ID                                 = 15 // <paramlist> ::= <type> ID <paramlist2>
| RULE_IFSTMT_IF_THEN_END                           = 16 // <if stmt> ::= if <exp> then <stmt seq> end
| RULE_IFSTMT_IF_THEN_ELSE_END                      = 17 // <if stmt> ::= if <exp> then <stmt seq> else <stmt seq> end
| RULE_REPEATSTMT_REPEAT_UNTIL                      = 18 // <repeat stmt> ::= repeat <stmt seq> until <exp>
| RULE_ARRAYASSIGNSTMT_ID_LBRACKET_RBRACKET_COLONEQ = 19 // <array assign stmt> ::= ID '[' <exp> ']' ':=' <exp>
| RULE_ASSIGNSTMT_ID_COLONEQ                        = 20 // <assign stmt> ::= ID ':=' <exp>
| RULE_ASSIGNSTMT                                   = 21 // <assign stmt> ::= <array assign stmt>
| RULE_FUNDEFSTMT_FUN_ID_LPAREN_RPAREN_END          = 22 // <fundef stmt> ::= fun ID '(' <paramlist> ')' <fundef ret type> <stmt seq> end
| RULE_FUNDEFRETTYPE                                = 23 // <fundef ret type> ::= 
| RULE_FUNDEFRETTYPE_COLON                          = 24 // <fundef ret type> ::= ':' <type>
| RULE_VARDEFSTMT_ID                                = 25 // <vardef stmt> ::= <type> ID
| RULE_VARDEFSTMT_ID_COLONEQ                        = 26 // <vardef stmt> ::= <type> ID ':=' <exp>
| RULE_ARGLIST2                                     = 27 // <arglist2> ::= 
| RULE_ARGLIST2_COMMA                               = 28 // <arglist2> ::= ',' <arglist>
| RULE_ARGLIST                                      = 29 // <arglist> ::= 
| RULE_ARGLIST3                                     = 30 // <arglist> ::= <exp> <arglist2>
| RULE_TYPEPARAMLIST2                               = 31 // <typeparamlist2> ::= 
| RULE_TYPEPARAMLIST2_COMMA                         = 32 // <typeparamlist2> ::= ',' <typeparamlist>
| RULE_TYPEPARAMLIST                                = 33 // <typeparamlist> ::= <type> <typeparamlist2>
| RULE_TYPEPARAMS_LBRACKETPIPE_PIPERBRACKET         = 34 // <typeparams> ::= '[|' <typeparamlist> '|]'
| RULE_TYPEPARAMS                                   = 35 // <typeparams> ::= 
| RULE_CALL_ID_LPAREN_RPAREN                        = 36 // <call> ::= ID <typeparams> '(' <arglist> ')'
| RULE_RETURNSTMT_RETURN                            = 37 // <return stmt> ::= return <exp>
| RULE_BOOL_TRUE                                    = 38 // <bool> ::= true
| RULE_BOOL_FALSE                                   = 39 // <bool> ::= false
| RULE_EXP_LT                                       = 40 // <exp> ::= <simple exp> '<' <simple exp>
| RULE_EXP_LTEQ                                     = 41 // <exp> ::= <simple exp> '<=' <simple exp>
| RULE_EXP_GT                                       = 42 // <exp> ::= <simple exp> '>' <simple exp>
| RULE_EXP_GTEQ                                     = 43 // <exp> ::= <simple exp> '>=' <simple exp>
| RULE_EXP                                          = 44 // <exp> ::= <simple exp>
| RULE_SIMPLEEXP_PLUS                               = 45 // <simple exp> ::= <simple exp> '+' <term>
| RULE_SIMPLEEXP_MINUS                              = 46 // <simple exp> ::= <simple exp> '-' <term>
| RULE_SIMPLEEXP                                    = 47 // <simple exp> ::= <term>
| RULE_TERM_TIMES                                   = 48 // <term> ::= <term> '*' <comp>
| RULE_TERM_DIV                                     = 49 // <term> ::= <term> '/' <comp>
| RULE_TERM                                         = 50 // <term> ::= <comp>
| RULE_COMP_EXCLAMEQ                                = 51 // <comp> ::= <comp> '!=' <factor>
| RULE_COMP_EQ                                      = 52 // <comp> ::= <comp> '=' <factor>
| RULE_COMP                                         = 53 // <comp> ::= <factor>
| RULE_FACTOR_LPAREN_RPAREN                         = 54 // <factor> ::= '(' <exp> ')'
| RULE_FACTOR_NUMBER                                = 55 // <factor> ::= Number
| RULE_FACTOR_ID                                    = 56 // <factor> ::= ID
| RULE_FACTOR_STRING                                = 57 // <factor> ::= String
| RULE_FACTOR                                       = 58 // <factor> ::= <bool>
| RULE_FACTOR2                                      = 59 // <factor> ::= <call>
| RULE_FACTOR3                                      = 60 // <factor> ::= <new array>
| RULE_FACTOR4                                      = 61 // <factor> ::= <array access>
| RULE_NEWARRAY_NEW_LBRACKET_RBRACKET               = 62 // <new array> ::= new <type> '[' <simple exp> ']'
| RULE_ARRAYACCESS_ID_LBRACKET_RBRACKET             = 63 // <array access> ::= ID '[' <exp> ']'


