﻿module CodeGen

open System 
open System.Reflection 
open System.Reflection.Emit 
open System.Diagnostics.SymbolStore
open System.Diagnostics
open System.Collections.Generic
open AST
open Result
open Checking

let getTypeBuilder n = 
    let currentDomain = AppDomain.CurrentDomain in
    let assemName  = new AssemblyName(n) in
    let assemBuilder = currentDomain.DefineDynamicAssembly(assemName, AssemblyBuilderAccess.RunAndSave) in
    let daType = typeof<DebuggableAttribute> in
    let daCtor = daType.GetConstructor([| typeof<DebuggableAttribute.DebuggingModes> |]) in
    let daBuilder = new CustomAttributeBuilder(daCtor, [| DebuggableAttribute.DebuggingModes.DisableOptimizations ||| DebuggableAttribute.DebuggingModes.Default |] ) in
        assemBuilder.SetCustomAttribute(daBuilder);
    let moduleBuilder = assemBuilder.DefineDynamicModule("MyModule", n, true) in
    let typeBuilder = moduleBuilder.DefineType("MyClass", TypeAttributes.Public) in
        (assemBuilder, typeBuilder)

type CodegenScope(rs:ResolvingScope, assemBuilder: AssemblyBuilder, typeBuilder: TypeBuilder, parent:CodegenScope option) =
    let rs = rs
    let functions = Dictionary<FunDef, MethodBuilder>()

    new(rs:ResolvingScope, parent:CodegenScope) = CodegenScope(rs, parent.assemBuilder, parent.typeBuilder, Some parent)
    new(rs:ResolvingScope, assemBuilder: AssemblyBuilder, typeBuilder: TypeBuilder) = CodegenScope(rs, assemBuilder, typeBuilder, None)

    member val parent: CodegenScope option = parent
    member val assemBuilder: AssemblyBuilder = assemBuilder
    member val typeBuilder: TypeBuilder = typeBuilder
    member val variables = Dictionary<ID, LocalBuilder>()
    member val arguments = Dictionary<ID, int>()

    member this.AddFunction f mb = functions.Add(f, mb)
    member this.TryGetFunction f =
        let functions' = functions
        printfn "%A" functions';
        match functions'.TryGetValue f with
        | true, mb -> Success mb
        | false, _ -> match parent with
                      | Some p -> p.TryGetFunction f
                      | None -> Failure("Could not resolve function")

    interface IScope with
        member x.CurrentFunction
            with get (): FunDef = (rs :> IScope).CurrentFunction
            and set (v: FunDef): unit = (rs :> IScope).CurrentFunction <- v
        member this.ResolveArgDecl r = failwith("not implemented")
        member this.ResolveVarDecl r = failwith("not implemented")
        member this.ResolveType r = (rs :> IScope).ResolveType r
        member this.ResolveVar r = failwith("not implemented")
        member this.ResolveFunction loc r = failwith("not implemented")
        member this.ResolveFunctionDef r = failwith("not implemented")


let emitBinOp (il:ILGenerator) (op:BinOp) =
    let negate() = il.Emit(OpCodes.Ldc_I4_0);il.Emit(OpCodes.Ceq)
    match op with
    | ADD -> il.Emit(OpCodes.Add)
    | SUB -> il.Emit(OpCodes.Sub)
    | MUL -> il.Emit(OpCodes.Mul)
    | DIV -> il.Emit(OpCodes.Div)
    | LT  -> il.Emit(OpCodes.Clt)
    | LTE -> il.Emit(OpCodes.Cgt); negate()
    | GT  -> il.Emit(OpCodes.Cgt)
    | GTE -> il.Emit(OpCodes.Clt); negate()
    | EQ  -> il.Emit(OpCodes.Ceq)
    | NE  -> il.Emit(OpCodes.Ceq); negate()

let rec buildExprIL (il:ILGenerator) (expr:Expr) (scope:CodegenScope) : Compiling<unit>  = 
    let e,et,loc = getTyped expr
    match e with
    | Boolean(true) -> il.Emit(OpCodes.Ldc_I4_1); Success ()
    | Boolean(false) -> il.Emit(OpCodes.Ldc_I4_0); Success ()
    | Literal(v) -> il.Emit(OpCodes.Ldc_I4, v); Success ()
    | String s -> il.Emit(OpCodes.Ldstr, s); Success ()
    | Var(Sym _) -> Failure(Codegen "unresolved")
    | Var(Val(t, id, kind)) ->
        match kind with
        | Local -> match scope.variables.TryGetValue id with
                   | true, lbd -> il.Emit(OpCodes.Ldloc, lbd); Success ()
                   | false, _ -> Failure(Codegen "unresolved")
        | Arg -> match scope.arguments.TryGetValue id with
                 | true, idx -> il.Emit(OpCodes.Ldarg, idx); Success ()
                 | false, _ -> Failure(Codegen "unresolved")
    | BinOp(op,a,b) -> result{  let! ca = buildExprIL il a scope
                                let! cb = buildExprIL il b scope
                                let cop = emitBinOp il op;
                                return()
                       }
    | Call(c) -> buildCallIL il scope c
    | NewArray(Val(tid,t),s) -> resultu { let size = buildExprIL il s scope
                                          il.Emit(OpCodes.Newarr, t); }
    | ArrayAccess(a,i) -> resultu { let arr = buildExprIL il a scope
                                    let idx = buildExprIL il i scope
                                    il.Emit(OpCodes.Ldelem_I1); }
    
and buildCallIL (il:ILGenerator) (scope:CodegenScope) c : Compiling<unit> =
    match c with
    | Sym _ -> Failure(Codegen "unresolved call sym")
    | Val(f,args,tparams) ->
        result{ 
            let! cargs = args |> List.map (fun e -> buildExprIL il e scope) |> Result.first
            match f.body with
            | Fun(bm, fScope) -> match scope.TryGetFunction f with
                                 | Success mi -> il.Emit(OpCodes.Call,  mi); return ()
                                 | Failure _ -> return! Failure(Codegen "method not found")
            | Prim(primBuilder) -> return primBuilder il
        }

and buildIL (il:ILGenerator) (scope:CodegenScope) (x:AstNode) : Compiling<unit> = 
    match x with
    | Stmts(l) -> l |> List.map(buildIL il scope) |> Result.first
    | Decl(Sym(tid, id)) -> failwith("unresolved sym")
    | Decl(Val((tid, vartype), id, kind)) ->
        let (ID varname) = id
        let localBd = il.DeclareLocal(vartype)
        localBd.SetLocalSymInfo(varname);
//        if vartype.IsArray then
//            il.Emit(OpCodes.Ldc_I4, 2);
//            il.Emit(OpCodes.Newarr, vartype.GetElementType());
//        else ();
        scope.variables.Add(id, localBd);
        Success ()
    | Assignment(Val(t, id, kind), e) -> 
        match scope.variables.TryGetValue id with
        | true, loc -> 
            let ctx = buildExprIL il e scope
            il.Emit(OpCodes.Stloc, loc)
            Success ()
        | false, _ -> failwith("no var")
    | ArrayAssignment(a,i,e) -> 
        resultu {
            let! ca = buildExprIL il a scope
            let! ci = buildExprIL il i scope
            let! ce = buildExprIL il e scope
            il.Emit(OpCodes.Stelem_I1)
        }
    | Expr(Typed(Call c, ct, loc)) ->
        let call = result { return! buildCallIL il scope c }
        if ct <> VoidType then
            il.Emit(OpCodes.Pop)
        call
        
    | IfThenElse(cond, ifTrue, None) -> // test; brfalse REST; IFTRUE: {...} REST:...
        resultu {
            let! ccond = buildExprIL il cond scope;
            let endLbl = il.DefineLabel()
            il.Emit(OpCodes.Brfalse, endLbl);
            let! cscope = buildIL il scope ifTrue;
            il.Emit(OpCodes.Br, endLbl);
            il.MarkLabel(endLbl);
        }

    | IfThenElse(cond, ifTrue, Some(ifFalse)) -> // test; brfalse IFFALSE; IFTRUE: {... BR REST} IFFALSE: {...} REST:...
        resultu {
            let! ccond = buildExprIL il cond scope
            let ifFalseLbl = il.DefineLabel()
            let endLbl = il.DefineLabel()
            il.Emit(OpCodes.Brfalse, ifFalseLbl);
            let! cit = buildIL il scope ifTrue;
            il.Emit(OpCodes.Br, endLbl);
                
            il.MarkLabel(ifFalseLbl);
            let! cif = buildIL il scope ifFalse;
            il.MarkLabel(endLbl);
        }
//                    ctx
    | RepeatUntil(cond, stmts) -> //START: test; brfalse END; {... br START}; END:
        let startL, endL = il.DefineLabel(), il.DefineLabel() in
            il.MarkLabel(startL);
            let ctx =  buildExprIL il cond scope
            il.Emit(OpCodes.Brtrue, endL);
            let ctx = buildIL il scope stmts
            il.Emit(OpCodes.Br, startL);
            il.MarkLabel(endL);
            Success ()
    | FunDef(Val({name=name} as f)) -> resultu { let! mb = buildFunctionIL scope f
                                                 return () }
    | Return(r) -> match r with
                   | Some(ret) -> buildExprIL il ret scope// il.Emit(OpCodes.Ret); 
                   | None -> Success ()// il.Emit(OpCodes.br);
    | _ -> Failure(Codegen (sprintf "Unknown value during codegen: %A" x))
and buildFunctionIL (scope:CodegenScope) ({name=ID(name); body=body; ret=ret; parameters=parameters} as fd:FunDef) : Compiling<MethodBuilder> =
    let retType = 
        result {
            match ret with
            | Val(t) when t = VoidType -> return null
            | Val(_,rt) -> return rt
            | Sym(rtid) -> 
                let! (_,rt) = (scope :> IScope).ResolveType ret
                return rt
        }
//    let aParams = parameters |> List.map (fun (Val((_,t),_,_)) -> t) |> Array.ofList
    let aParams = parameters |> List.map (fun (Val((_,t),_,_)) -> t) |> Array.ofList
    match retType with
    | Failure _ -> Failure(Codegen "Could not gen return type")
    | Success rt ->
        let methodBuilder = scope.typeBuilder.DefineMethod(name, MethodAttributes.Public ||| MethodAttributes.Static, rt, aParams)
        scope.AddFunction fd methodBuilder;

        let il = methodBuilder.GetILGenerator()
        match body with
        | Fun(body, (:? ResolvingScope as fScope)) ->
            let fCGScope = CodegenScope(fScope, scope)
            parameters |> List.iteri (fun i (Val(_,id,k)) -> fCGScope.arguments.Add(id, i))
            result {
                do! buildIL il fCGScope body;
                il.Emit(OpCodes.Ret);
                return methodBuilder
            }
        | Prim(_) -> Failure(Codegen "trying to build a method info")
        | _ -> Failure(Codegen "???")
    
let compileFunction (assemBuilder:AssemblyBuilder) (typeBuilder:TypeBuilder) (x:AstNode): Compiling<MethodInfo> =
    match x with
    | FunDef(Val({name=ID(name)} as fd)) ->
        result {
            let! rs = match fd.body with
                      | Fun(_, (:? ResolvingScope as rs)) -> Success rs
                      | Prim _ -> Failure(Codegen "Cannot compile a primitive")
            let scope = CodegenScope(rs, assemBuilder, typeBuilder)
            let! mainMI = buildFunctionIL scope fd
            let t = typeBuilder.CreateType()
            let mi = t.GetMethod(name) in
                assemBuilder.SetEntryPoint(mi);
                return mi
        }
    | _ -> Failure(Codegen "Cannot codegen this ast, root node is not a fundef")
                    