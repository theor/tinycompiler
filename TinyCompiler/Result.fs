﻿module Result


type Result<'TSuccess,'TFailure> = 
    | Success of 'TSuccess
    | Failure of 'TFailure

let isSuccess = function
    | Success _ -> true
    | Failure _ -> false
    
let getSuccess = function
    | Success s -> s
    | Failure _ -> failwith("FAILURE")
let ofOption err = function
    | None -> err
    | Some s -> Success s

let isFailure x = isSuccess x = false

let bind switchFunction twoTrackInput = 
    match twoTrackInput with
    | Success s -> switchFunction s
    | Failure f -> Failure f

let switch f x = 
    f x |> Success


let map (oneTrackFunction: 'a -> 'b) (twoTrackInput: Result<'a,'c>): Result<'b,'c> = 
    match twoTrackInput with
    | Success s -> Success (oneTrackFunction s)
    | Failure f -> Failure f
    
let map2 oneTrackFunction twoTrackInput1 twoTrackInput2 = 
    match twoTrackInput1, twoTrackInput2 with
    | Success s1, Success s2 -> Success (oneTrackFunction s1 s2)
    | Failure f, Success _ | Success _, Failure f -> Failure f
    | Failure f, Failure f2 -> Failure f

//let rfor oneTrackFunction seq = 

let (>=>) switch1 switch2 x = 
    match switch1 x with
    | Success s -> switch2 s
    | Failure f -> Failure f 

let fold (f: 'a -> Result<'b, 'c>) (red:'b list -> 'b) (l: 'a list) : Result<'b, 'c> =
//    let rec fold_rec = function
//    | [] -> []
//    | h :: t -> match h with
//                | Success(s) -> 
//                | Failure(f) -> Failure f 
     let resList = l |> List.map f // |> fold_rec
     match List.tryFind isFailure resList with
     | Some f -> f
     | None -> resList |> List.map getSuccess |> red |> Success

type ResultBuilder() = 
    member this.Bind(x, f) = bind f x
    member this.Return(x) = Success(x)
    member this.ReturnFrom(x) = x
    member this.Yield(x) = Success(x)
    member this.Delay(f) = f()
    member this.For(seq, f) = this.Bind(seq, f)
    member this.TryWith(a,b) = a
type UnitResultBuilder() = 
    inherit ResultBuilder()
    member this.Zero() = Success ()
let result = ResultBuilder()
let resultu = UnitResultBuilder()


let rec first l = match l with
                    | Success () :: t -> 
                        result {
                            let! rest = first t
                            return rest
                        }
                    | Failure f :: t -> Failure f
                    | [] -> Success()
let rec flatten l = match l with
                    | Success h :: t -> 
                        result {
                            let! rest = flatten t
                            return h ::rest
                        }
                    | Failure f :: t -> Failure f
                    | [] -> Success([])

let test () =
    printfn "test";
    ()