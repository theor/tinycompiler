﻿module AST

open System
open System.Reflection
open Result
open System.Reflection.Emit

[<StructuredFormatDisplayAttribute("ToString")>]
type Location = 
    { column : int
      line : int
      position : int }
      override this.ToString() = sprintf "(%i:%i)" (this.line+1) (this.column+1)
let NoLoc:Location = { column=0; line=0; position=0 }

type ID = 
    | ID of string
    with 
      override this.ToString() = match this with | ID(s) -> s

type TID = 
    | Type of string
    | Array of TID
    with 
      override this.ToString() = match this with | Type(s) -> s | Array t -> t.ToString() + "[]"

type Type = TID * System.Type

let AnyType : Type = (Type("*"), typeof<System.Object>)
let VoidType : Type = (Type("void"), typeof<unit>)
let IntType : Type = (Type("int"), typeof<int>)
let BoolType : Type = (Type("bool"), typeof<bool>)
let StringType : Type = (Type("string"), typeof<string>)

let MakeArrayType (t:Type) =
    match t with
    | (Type(tid),nt) -> (Type(tid + "[]"),nt.MakeArrayType())
//    | (Type(tid),nt) -> t
//    | (Array(Type(tid)),nt) -> (Type(tid + "[]"), typeof<Array>.MakeGenericType(nt))
    | _ -> failwith("unres array sub type")

type LocationError = string * Location
type Parsing<'a> = Result<'a, LocationError>
type Resolving<'a> = Result<'a, LocationError>

type CompilingError = Parsing of LocationError | Resolving of LocationError | Codegen of string | Invocation of string
type Compiling<'a> = Result<'a, CompilingError>

type Resolvable<'a, 'b> = 
    | Sym of 'a
    | Val of 'b

let rec getVal r = 
    match r with
    | Val v -> Success v
    | Sym s -> Failure(sprintf "Unresolved symbol %A" s, NoLoc)

and getSym (r : Resolvable<'a, 'b>) = 
    match r with
    | Val v -> Failure("Already resolved symbol", None)
    | Sym s -> Success s

type VarDeclSym = TID * ID

and VarKind = 
    | Local
    | Arg

and 
    ///<summary>VarDecl: Type,ID,Kind</summary>
    VarDecl = Type * ID * VarKind

and CallSym = ID * Expr list * TID list

and CallVal = FunDef * Expr list * TID list

and PrimBuilder = ILGenerator -> unit 
and  [<CustomEquality;NoComparison>]
     FunSrc = 
    | Fun of AstNode * IScope
    | Prim of PrimBuilder
    override x.Equals(yobj) = true
    override x.GetHashCode() = 0

and     
    [<StructuredFormatDisplayAttribute("ToString");CustomEquality;NoComparison>]
    FunDef = 
    { name : ID
      parameters : Resolvable<VarDeclSym, VarDecl> list
      typeParams: TID list
      ret : Resolvable<TID, Type>
      body : FunSrc
      uid: int }
    with 
      override x.Equals(yobj) =
        match yobj with
        | :? FunDef as y -> x.uid = y.uid
        | _ -> false
      override x.GetHashCode() = x.uid
      override this.ToString() =
        
        let parms = this.parameters
                    |> List.map (fun p -> match p with
                                          | Val((t,_),id,_) -> sprintf "%O %O" t id
                                          | Sym(t,id) -> sprintf "%O %O" t id )
        let sret = match this.ret with | Val(t,_) -> t | Sym(t) -> t
        sprintf "%O(%O):%O" this.name (String.Join(", ",parms)) sret

and BinOp = 
    | LT
    | LTE
    | GT
    | GTE
    | EQ
    | NE
    | ADD
    | SUB
    | DIV
    | MUL

and ExprValue = 
    | String of string
    | Boolean of bool
    | Literal of int
    | Var of Resolvable<ID, VarDecl>
    | BinOp of BinOp * Expr * Expr
    | Call of Resolvable<CallSym, CallVal>
    | NewArray of Resolvable<TID, Type> * Expr
    | ArrayAccess of Expr * Expr

and Expr = 
    | Untyped of ExprValue * Location
    | Typed of ExprValue * Type * Location

and [<StructuredFormatDisplay("SFD")>] AstNode = 
    //| Program of AstNode * IScope
    | Stmts of AstNode list
    | Expr of Expr
    | Assignment of Resolvable<ID, VarDecl> * Expr
    | ArrayAssignment of Expr * Expr * Expr
    | Decl of Resolvable<VarDeclSym, VarDecl>
    | FunDef of Resolvable<FunDef, FunDef>
    | IfThenElse of Expr * AstNode * AstNode option
    | RepeatUntil of Expr * AstNode
    | Return of Expr option

//and Resolver<'a, 'b> = Resolvable<'a, 'b> -> Resolving<'b>
and IScope = 
    abstract CurrentFunction:FunDef with get,set
    abstract ResolveType : Resolvable<TID, Type> -> Resolving<Type>
    abstract ResolveVarDecl : Resolvable<VarDeclSym, VarDecl> -> Resolving<VarDecl>
    abstract ResolveArgDecl : Resolvable<VarDeclSym, VarDecl> -> Resolving<VarDecl>
    abstract ResolveVar : Resolvable<ID, VarDecl> -> Resolving<VarDecl>
    abstract ResolveFunction : Location -> Resolvable<CallSym, CallVal> -> Resolving<CallVal>
    abstract ResolveFunctionDef : Resolvable<FunDef, FunDef> -> Resolving<FunDef>

and EmptyScope() = 
    interface IScope with
        member x.CurrentFunction
            with get (): FunDef = 
                failwith "Not implemented yet"
            and set (v: FunDef): unit = 
                failwith "Not implemented yet"
        
        member this.ResolveVarDecl r = failwith ("not implemented")
        member this.ResolveArgDecl r = failwith ("not implemented")
        member this.ResolveType r = failwith ("not implemented")
        member this.ResolveVar r = failwith ("not implemented")
        member this.ResolveFunction _ r = failwith ("not implemented")
        member this.ResolveFunctionDef r = failwith ("not implemented")

let getTyped r = 
    match r with
    | Untyped(u,loc) -> failwith("Untyped expr")
    | Typed(e,t,loc) -> e, t, loc
let tryGetTyped r = 
    match r with
    | Untyped(u,loc) -> Failure("Untyped expr", NoLoc)
    | Typed(e,t,loc) -> Success(e, t, loc)
let getType r = result { let! e,t,loc = tryGetTyped r
                         return t }

let defScope = EmptyScope()

type ErrorTypes =
| WrongNumberOfArguments of FunDef * int
| BadArgType of FunDef * ID * Type * Type
///<summary>Bad Assignment: var id, expected type, actual type</summary>
///<param name="var id">var id</param>
///<param name="expected">var id</param>
///<param name="actual">var id</param>
| BadAssignmentType of ID * Type * Type
| ReturnValueInVoidFunction of FunDef
| BadReturnValueType of FunDef * Type * Type
| BadOpOperandType of BinOp * (Type * Type) option * (Type * Type) option 
| UnknownFunction of ID
| NoMatchingOverload of FunDef list
| ArrayIndexType of Type

let expectedStr str e a = sprintf "\texpected %s: %O\n\t  actual %s: %O\n" str e str a
let expected e a = expectedStr "" e a
let typeMismatchStr str (e,_) (a,_) = sprintf "%stype mismatch:\n%s" str (expected e a)
let typeMismatch = typeMismatchStr ""
let badOperand indexStr operand = 
    match operand with
    | None -> String.Empty
    | Some(e, a) -> (typeMismatchStr (indexStr + " operand ") e a)
let error (l:Location) (e:ErrorTypes) =
    let s = match e with
            | WrongNumberOfArguments(fd, actualNumber) -> sprintf "Call to function %O: wrong number of arguments%s" fd.name (expected fd.parameters.Length actualNumber)
            | BadArgType(f,name,expected,actual) -> sprintf "Call to function %O: argument '%O' %s" f.name name (typeMismatch expected actual)
            | BadAssignmentType(name,expected,actual) -> sprintf "Assignment to variable %O: %s" name (typeMismatch expected actual)
            | BadReturnValueType(f,expected,actual) -> sprintf "Assignment to variable %O: %s" f.name (typeMismatch expected actual)
            | ReturnValueInVoidFunction(f) -> sprintf "In void function %O: trying to return a value" f.name
            | BadOpOperandType(op, op1, op2) -> sprintf "Operator %A has bad operand(s) type(s)\n%s%s" op (badOperand "First" op1) (badOperand "Second" op2)
            | UnknownFunction(f) -> sprintf "Could not resolve fun '%O'" f
            | NoMatchingOverload defs ->  sprintf "No matching overload for this function, overloads:\n\t%s" (defs |> List.map (sprintf "%O") |> List.fold (fun a b -> a + b + "\n\t") "" )
            | ArrayIndexType t -> sprintf "Array index should be int, is %O" t
    Failure(s, l)


let fillLoc (src:string) (loc:Location) =
    if loc = NoLoc then "?:? ..."
    else
        let lines =  src.Split([| '\n' |])
        let line = lines.[loc.line]
        let marker = "^".PadLeft(loc.column+1)
        sprintf "\t%s\n\t%s" line marker