﻿module Parser
open Parsing

open System
open System.IO
open System.Runtime.Serialization
open com.calitha.goldparser.lalr
open com.calitha.commons

open Result
open AST

let loc (l:com.calitha.goldparser.Location):Location = { column=l.ColumnNr; line=l.LineNr; position=l.Position }

type RC = RuleConstants
type SC = SymbolConstants

let mutable fuid = 100

type Token =
| NonterminalToken of RuleConstants * List<Token>
| TerminalToken of SymbolConstants * string * com.calitha.goldparser.Location

let rec at i (l: 'a List) = match l with
                            | [] -> failwith("list too short")
                            | h::t -> if i = 0 then h else (at (i - 1) t)

let rec getSymbol(t : Token) : string = match t with
                                        | TerminalToken(SC.SYMBOL_ID, s, _) -> s
                                        | NonterminalToken(_, t) -> if t.Length <> 1 then failwith("not a valid symbol a") else getSymbol (List.head t)
                                        | _ -> failwith("not a valid symbol a")
let rec getLoc (t:Token): Location =
    match t with
    | TerminalToken(_, _, l) -> loc l
    | NonterminalToken(_, ts) -> ts |> List.head |> getLoc

let tryGetID(t : Token) : Parsing<ID * Location> =
    match t with
    | TerminalToken(SC.SYMBOL_ID, s, l) -> Success(ID s, loc l)
    | _ -> let loc = getLoc t
           Failure("not a valid symbol", loc)
let rec tryGetTID(t : Token) : Parsing<TID * Location> =
    match t with
    | TerminalToken(SC.SYMBOL_ID, s, l) -> Success(Type s, loc l)
    | NonterminalToken(RC.RULE_TYPE_ID, t) -> t |> List.head |> tryGetTID // Success(Type s, loc l)
    | NonterminalToken(RC.RULE_TYPE_ID_LBRACKETRBRACKET, subtype :: _ :: []) ->
        result {
            let! st,loc = tryGetTID subtype
            return Array(st),loc
        }
    | _ -> Failure("not a valid symbol", getLoc t)

let tryGetOp(s:string) l: Parsing<BinOp> =
    match s with
    | ">" -> Success GT
    | ">=" -> Success GTE
    | "<" -> Success LT
    | "<=" -> Success LTE
    | "<>" -> Success NE
    | "=" -> Success EQ
    | "+" -> Success ADD
    | "-" -> Success SUB
    | "/" -> Success DIV
    | "*" -> Success MUL
    | _ -> Failure(sprintf "Unknown op: %A" s, l)
    

//let rec getTerm(t:Token): Parsing<Token> =
//    match t with
//    | NonterminalToken(r, l) -> if List.length l = 1 then getTerm(List.head l) else Failure("Several tokens", getLoc t)
//    | TerminalToken(_,_,_) -> Success t

let rec getExpr(t : Token) : Parsing<Expr> =
    match t with
    | TerminalToken(SC.SYMBOL_NUMBER, s, l) -> Success(Untyped(Literal(int32(s)),loc l))
    | TerminalToken(SC.SYMBOL_ID, s, l) -> Success(Untyped(Var(Sym(ID s)),loc l))
    | TerminalToken(SC.SYMBOL_TRUE, s, l) -> Success(Untyped(Boolean true,loc l))
    | TerminalToken(SC.SYMBOL_FALSE, s, l) -> Success(Untyped(Boolean false,loc l))
    | TerminalToken(SC.SYMBOL_STRING, s, l) -> Success(Untyped(String (s.Trim('"')),loc l))

    
    | NonterminalToken(RC.RULE_NEWARRAY_NEW_LBRACKET_RBRACKET, l) ->
        match l with
        | newkw :: t :: _ :: size :: _ :: [] ->
            result {
                let! tid,lts = tryGetTID t
                let! e = getExpr size
                return Untyped(NewArray(Sym(tid), e), getLoc newkw)
            }
        | _ -> Failure("Bad new array expression", getLoc t)
    | NonterminalToken(RC.RULE_FACTOR_LPAREN_RPAREN, _ :: x :: _ :: []) -> getExpr x
    | NonterminalToken(RC.RULE_TERM, ts)
    | NonterminalToken(RC.RULE_FACTOR, ts)
    | NonterminalToken(RC.RULE_FACTOR2, ts)
    | NonterminalToken(RC.RULE_EXP, ts)
    | NonterminalToken(RC.RULE_COMP, ts)
    | NonterminalToken(RC.RULE_SIMPLEEXP, ts) -> List.head ts |> getExpr
    | NonterminalToken(RC.RULE_CALL_ID_LPAREN_RPAREN, ts) ->
        match ts with
        | TerminalToken(SC.SYMBOL_ID, fname, l) :: _ :: _ :: args :: _ :: [] ->
            result {
                let! eArgs = getArgs args
                return Untyped(Call(Sym(ID(fname), eArgs, [])), loc l)
            }
        | _ -> Failure("RULE_CALL_ID_LPAREN_RPAREN", getLoc t)
    | NonterminalToken(RC.RULE_ARRAYACCESS_ID_LBRACKET_RBRACKET, ts) -> 
        match ts with
        | arr :: _ :: idx :: _ :: [] ->
            result {
                let! earr = getExpr arr
                let! eidx = getExpr  idx
                return Untyped(ArrayAccess(earr, eidx), getLoc arr)
            }
        | _ -> Failure("RULE_ARRAYACCESS_ID_LBRACKET_RBRACKET", getLoc t)
    | NonterminalToken(r,ts) ->
        match Seq.toList(ts) with
        | x :: [] -> x |>  getExpr
                
        | a :: TerminalToken(_, s, l) :: b :: [] -> result {
                let! ea = getExpr a
                let! eb = getExpr b
                let! bop = tryGetOp s (loc l)
                return Untyped(BinOp(bop, ea, eb),loc l)
            }
        | _ -> Failure("invalid expr", getLoc t)
    | _ -> failwith("a")

and getParams (t:Token):Resolvable<VarDeclSym,VarDecl> list =
    match t with
    | NonterminalToken(RC.RULE_PARAMLIST_ID, t :: v :: rest :: []) -> Sym(Type(getSymbol t), ID(getSymbol v)) :: getParams rest
    | NonterminalToken(RC.RULE_PARAMLIST2_COMMA, _ :: rest :: []) -> getParams rest
    | _ -> []
and getArgs (t:Token):Parsing<Expr list> =
    match t with
    | NonterminalToken(RC.RULE_ARGLIST2_COMMA, _ :: args :: []) -> getArgs args
    | NonterminalToken(RC.RULE_ARGLIST3, arg :: rest :: []) -> // getExpr arg :: getArgs rest
        result {
            let! eArg = getExpr arg
            let! eRest = getArgs rest
            return eArg :: eRest
        }
    | _ -> Success([])
and getTypeParams (t:Token): Parsing<TID list> = 
    match t with
    | NonterminalToken(RC.RULE_TYPEPARAMS_LBRACKETPIPE_PIPERBRACKET, _ :: l :: _ :: []) -> getTypeParams l
    | NonterminalToken(RC.RULE_TYPEPARAMLIST, t :: rest :: []) -> result { let! tid,loc = tryGetTID t
                                                                           let! trest = getTypeParams rest
                                                                           return tid :: trest }
    | NonterminalToken(RC.RULE_TYPEPARAMS, []) -> Success []
    | NonterminalToken(RC.RULE_TYPEPARAMLIST2, []) -> Success []
    | _ -> failwith("wtf getTypeParams")


type MyParser(filename : string) as self =
    [<DefaultValue>]val mutable parser : com.calitha.goldparser.LALRParser
    let mutable error: LocationError option = None
    do
        let stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read)
        self.Init(stream)
//        com.calitha.goldparser.LALRParser.
        let onError (x:com.calitha.goldparser.LALRParser) (e:com.calitha.goldparser.ParseErrorEventArgs) =
            error <- Some(sprintf "Got token '%A', expected '%A'" e.UnexpectedToken e.ExpectedTokens, loc e.UnexpectedToken.Location)
        let onTokenError (x:com.calitha.goldparser.LALRParser) (e:com.calitha.goldparser.TokenErrorEventArgs) =
            error <- Some(sprintf "Unexpected token '%A'" e.Token, loc e.Token.Location)
        self.parser.add_OnParseError(new com.calitha.goldparser.LALRParser.ParseErrorHandler(onError))
        self.parser.add_OnTokenError(new com.calitha.goldparser.LALRParser.TokenErrorHandler(onTokenError))
        stream.Close()

    member public this.Init (stream : Stream) =     
        let reader = new com.calitha.goldparser.CGTReader(stream)
        this.parser <- reader.CreateNewParser()
        this.parser.TrimReductions <- false
        this.parser.StoreTokens <- com.calitha.goldparser.LALRParser.StoreTokensMode.NoUserObject

        //parser.OnTokenError += new LALRParser.TokenErrorHandler(TokenErrorEvent);
        //parser.OnParseError += new LALRParser.ParseErrorHandler(ParseErrorEvent);


    member this.CreateObject(token : Token) : Parsing<AstNode> =
        
        match token with
        | TerminalToken(symbol,text, l) -> Failure(sprintf "Unknown symbol %A" text, loc l)
        | NonterminalToken(rule,tokens) -> 
            match rule with
            | RC.RULE_PROGRAM -> result {
                    let! program = tokens |> List.map this.CreateObject |> Result.flatten
                    return FunDef(Sym({ uid=0
                                        name=ID("main")
                                        parameters=[]
                                        typeParams=[]
                                        ret=Val(VoidType)
                                        body=Fun(Stmts(program), defScope)}))
                }
//                match tokens with
//                | x :: []))
//                Result.map (fun x -> Stmts(x :: [])) (tokens |> List.head |> this.CreateObject)// Success(Stmts(tokens |> List.map this.CreateObject))
            | RC.RULE_STMTSEQ_SEMI | RC.RULE_STMT4 ->
                match tokens with
                | a :: _ :: b :: [] -> result {
                        let! sa = this.CreateObject a
                        let! sb = this.CreateObject b
                        return Stmts([sa;sb])
                    }
                | a :: [] -> this.CreateObject a
                | _ -> Failure("", getLoc token)
            | RC.RULE_VARDEFSTMT_ID
            | RC.RULE_VARDEFSTMT_ID_COLONEQ          ->
                match tokens with
                | t :: id :: [] ->
                    result {
                        let! tid,lts = tryGetTID t
                        let! ids,lids = tryGetID id
                        return Decl(Sym(tid,ids))
                    }
                | t :: id :: _ :: v :: [] ->
                    result {
                        let! tid,lts = tryGetTID t
                        let! ids,lids = tryGetID id
                        let! ve = getExpr v
                        return Stmts([Decl(Sym(tid,ids));
                                      Assignment(Sym(ids), ve)])
                    }
                | _ -> failwith("invalid")
            | RC.RULE_FUNDEFSTMT_FUN_ID_LPAREN_RPAREN_END -> 
                match tokens with
                | sFUN :: fname :: _ :: lparams :: _ :: ret :: body :: sEND ->
                    let retTID = match ret with
                                 | NonterminalToken(RC.RULE_FUNDEFRETTYPE_COLON, _ :: t :: []) -> (Sym(Type(getSymbol t)))
                                 | _ -> Val VoidType
                    result {
                        let sfname = getSymbol fname
                        let parameters = getParams lparams
                        let! sBody = this.CreateObject(body)
                        return FunDef(Sym({uid= (fuid <- fuid+1; fuid)
                                           name = ID(sfname)
                                           parameters = parameters
                                           typeParams=[]
                                           ret =retTID
                                           body = Fun(sBody, EmptyScope())}))
                    }
                | _ -> Failure("", getLoc token)
            | RC.RULE_IFSTMT_IF_THEN_END                  -> 
                match tokens with
                | sIF :: cond :: sTHEN :: ifTrue :: sEND :: [] ->
                    result {
                        let! exprCond = getExpr cond
                        let! sIfTrue = this.CreateObject ifTrue
                        return IfThenElse(exprCond, sIfTrue, None)
                    }
                | _ -> Failure("", getLoc token)
            | RC.RULE_IFSTMT_IF_THEN_ELSE_END ->
                match tokens with
                | sIF :: cond :: sTHEN :: ifTrue :: sELSE :: ifFalse :: sEND :: [] ->
                    result {
                        let! exprCond = getExpr cond
                        let! sIfTrue = this.CreateObject ifTrue
                        let! sIfFalse = this.CreateObject ifFalse
                        return IfThenElse(exprCond, sIfTrue, Some sIfFalse)
                    }
                | _ -> Failure("", getLoc token)
//                let exprCond = getExpr cond in
//                    IfThenElse(exprCond, this.CreateObject ifTrue, Some(this.CreateObject ifFalse))
//xp>
            | RC.RULE_REPEATSTMT_REPEAT_UNTIL             ->
                match tokens with
                | _repeat :: stmts :: _until :: cond :: [] ->
                    result {
                        let! exprCond = getExpr cond
                        let! stmts = this.CreateObject stmts
                        return RepeatUntil(exprCond, stmts)
                    }
                | _ -> failwith("RULE_REPEATSTMT_REPEAT_UNTIL")
            | RC.RULE_ASSIGNSTMT_ID_COLONEQ               ->
                match tokens with
                | id :: _ :: v :: [] ->
                     result {
                        let! ids,lids = tryGetID id
                        let! ve = getExpr v
                        return Assignment(Sym(ids), ve)
                    }
                | _ -> failwith("RULE_ASSIGNSTMT_ID_COLONEQ")
            | RC.RULE_ARRAYASSIGNSTMT_ID_LBRACKET_RBRACKET_COLONEQ ->
                match tokens with
                | arr :: _lb :: idx :: _rb :: _eq :: v :: [] ->
                    result {
                        let! earr = getExpr arr
                        let! eidx = getExpr idx
                        let! ev = getExpr v
                        return ArrayAssignment(earr, eidx, ev);
                    }
                | _ -> failwith("RULE_ARRAYASSIGNSTMT_ID_LBRACKET_RBRACKET_COLONEQ")

            | RC.RULE_CALL_ID_LPAREN_RPAREN               ->
                match tokens with
                | fname ::typeparams :: _ :: args :: _ :: [] -> result {
                        let! tparams = getTypeParams typeparams
                        let! fid,floc = tryGetID fname
                        let! args = getArgs args
                        return Expr(Untyped(Call(Sym(fid, args, tparams)), floc))
                     }
                | _ -> Failure("RULE_CALL_ID_LPAREN_RPAREN", getLoc token)
            | RC.RULE_RETURNSTMT_RETURN  ->
                match tokens with
                | _ :: e :: []  -> e |> getExpr |> Result.map  Some |> Result.map Return
                | _ -> Failure("RULE_RETURNSTMT_RETURN", getLoc token)
            | _ -> if List.length tokens = 1 then this.CreateObject tokens.Head else Failure("more than on etoken", getLoc token)

    member this.Parse(source : string) : Compiling<AstNode> =
        let rec ToX (token : com.calitha.goldparser.Token)  : Result<Token,LocationError> =
            match token with
                | :? com.calitha.goldparser.TerminalToken as t -> Success (TerminalToken(enum t.Symbol.Id,t.Text, t.Location))

                | :? com.calitha.goldparser.NonterminalToken as t ->
                    result {
                        let! xtokens = t.Tokens |> List.ofArray |> List.map ToX |> Result.flatten
                        return NonterminalToken(enum t.Rule.Id, xtokens)
                     }

                | _ -> match error with
                       | None -> Failure("Unknown error during parsing",  NoLoc)
                       | Some e -> Failure e

        let parsingResult = result {
            let token = this.parser.Parse(source)
            let! xtoken = ToX(token)
            let obj = this.CreateObject(xtoken)
            return! obj
        }
        match parsingResult with
        | Success s -> Success s
        | Failure pe -> Failure (Parsing pe)
